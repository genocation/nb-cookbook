# Cookbook

This notebook repo contains the culinary experiments of a couple of computer geeks. That's why they
are in markdown, on git, and each recipe has a changelog.

We sincerely apologise.

## Index 

* [Sourdough pizza](./sourdough-pizza.md)
* [Chinese dumplings](./chinese-dumplings.md)
* [Flatbreads with mole](./flatbreads-with-mole.md)
* [Malai kofta with naan](./malai-kofta-with-naan.md)
* [Empanada de atún](./empanada-atun.md)
* [Tadka dal](./tadka-dal.md)
* [Tomato chutney](./tomato-chutney.md)
* [Soy and vegetables bolognese](./soy-vegetables-bolognese.md)
* [Purple potato patties](./purple-potato-patties.md)
* [Wild garlic pesto](./wild-garlic-pesto.md)
