# Chinese Dumplings 

Craving Chinese food to share with a vegan house, we decide to make our own vegan version of our
favorite Chinese takeaway dishes: chow mein, sweet chili tofu and dumplings.

For the dumplings we make two different vegan fillings and a dipping sauce.

`#chinese` `#dumplings` `#vegan`


## Ingredients

**For 24 huge dumplings**

For the dough:
* Flour (4 cups, 500gr)
* Salt (1 teaspoon)
* Warm water (1 1/4 cups, 300ml)

For the filling:
* Cabbage (2 cups, 200g)
* Spring onion (2 cups, 300g)
* Mushrooms
* Carrots
* Leeks
* Garlic (6 cloves)
* Ginger (4 tablespoons)
* Soy sauce
* Sesame oil
* Pepper

For the dipping sauce:
* Soy sauce (1/4 cup, 60ml)
* Rice wine vinegar (1/4 cup, 60ml)
* Sesame oil (1 teaspoon)
* Crushed red pepper flakes (1 teaspoon)

## Method 

1. Mix the flour and the salt, add the warm water and knead until it's flexible. You can leave it
   to rest at this moment.

2. While the dough rests, thinly cut the cabbage, the spring onion, the garlic and the ginger, and
   mix it together with soy sauce, sesame oil and pepper. This will be the base of your dumpling
   filling, other ingredients can be added to this.

3. On another bowl we mixed thinly sliced leek and Shitake mushrooms, this would make one
   filling.

4. Another filling consisted of thinly cut carrot and mushrooms, slightly cooked to make the
   carrot a bit soft.

5. Divide the cabbage mix by half and mix each with the other filling mixes. Keep two bowls
   prepared to fill up the dumplings.

6. Get the dough, roll into a cylinder and cut the rolls into half three times. This will result in
   8 small dough cylinders. Each one can be cut into three equal slices to make 24 dumplings.

7. Roll each small dough piece into a ball, flatten it with your hand making sure that it's
   proportionate and properly round, and then flatten it properly with a rolling pin. It should be
   thin and big enough to fill it up with a spoon full of good stuff.

8. The fun step! Making the folds. Place a spoon of filling in the center (try to make it as dry as
   possible), spread some water in the dough border where its sides should stick, and start folding.
   The technique is better observed in [this video](https://tasty.co/recipe/homemade-dumplings), but
   consists on leaving one side unfolded while the other side makes folds and gets stuck to the
   unfolded side. This way, the figure that will come out is moon-shaped. Place them on a tray
   which is covered in flour so that they don't stick.

![Chinese dumpling folding technique](./assets/chinese-dumplings-1.jpg)
![Chinese dumplings](./assets/chinese-dumplings-2.jpg)

9. Once they are all formed, place some oil in a hot pan, and place the dumplings. Wait until they
   are properly fried, golden and crunchy. Once that is done, add a bit of water (1/4 of a cup)
   into the hot pan and cover it with a lid. Leave them to steam. Once the water is gone, they
   are ready.

## Changelog

#### First attempt: _2021-01-16_

* They were absolutely delicious. We made the two fillings described in the recipe and they were
both amazing, although the leak and shitake was the winner. 

* They were probably too big, next time instead of making 24, we should try to make 32 by
dividing the last rolls into 4 instead of 3 equal pieces.

