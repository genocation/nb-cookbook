# Flatbreads with mole 

We perfected the flatbread technique on our van journey around Wales during the summer of 2020, but
it's always nice to find new things to eat those flatbreads with. Here's a Mexican-inspired
mole-like vegan thing that was delicious to combine with the breads, as well as the ultimate
flatbread recipe. 

`#flatbread` `#chapatti` `#mole` `#vegan` 


## Ingredients

**For 4 people/8 flatbreads**

For the flatbread dough:
* Flour (400gr)
  * 1/2 wholewheat
  * 1/2 white flour
* Water (200ml, warm)
* Salt (1 teaspoon)
* Olive oil (four table spoons)

For the vegan mole:
* 2 onions
* 2 peppers (red and yellow)
* Half a package of vegan mince
* A cup of dried fruits (walnuts, almonds, hazelnuts... whatever is available)
* Cocoa powder
* Chilli flakes
* White flour (1 large spoon)
* Vinegar
* Dark soy sauce

For the salsa "vinagreta":
* One red onion
* A small piece of red pepper (or half a tomato)
* A bunch of celery
* Chilly flakes
* A lemon or a lime
* Vinegar
* Olive oil


## Method 

1. For the flatbreads, mix the flours and the salt, add the water (slightly warm) and knead for a
   while, until the dough feels like it doesn't break too much and starts sticking together. Add the
     olive oil and keep on kneading until it's completely incorporated and the dough doesn't feel
     oily. Make a ball with it and cover it with some plastic wrap, leave it to rest until we are
     ready to cook the breads.

2. Cut the onions and the peppers in thin slices and saute them on a bit of oil. Keep stirring and
   maybe add bits of water if it sticks to the pan (or beer/wine if you have it around). After 10-15
   minutes (depending on how soft you want the onions and peppers) add the mince and keep on
   stirring.

4. When the consistency of the vegetables is what you want, add a spoon full of white flour, mix it
   with the vegetables and roast it a bit, and then add half a cup of water so that it starts to
   thicken.

3. On a separate pan or pot, roast the ground nuts. When they are completely roasted and smell rich,
   throw them into the vegetables and mix it all together.

4. Add two table spoons of cocoa powder, a teaspoon of chilli flakes, and stir it all together. It
   should start forming a creamy dark sauce. Keep on stirring and add water if the mix gets too dry.

5. Add a bit of vinegar and a bit of dark soy sauce. Keep on trying the mix and season it with
   vinegar and soy sauce until you've found the proper punch.

6. For a nice salsa or vinagreta, mince really thinly a whole red onion, half tomato or red pepper,
   a decent bunch of celery, and mix it all together. Season it with lemon juice, vinegar, olive
   oil, chilli flakes and salt. Leave this to rest for a while so that the onion marinates in the
   lemon juice.

7. Divide the dough ball into 8 or 10 smaller balls. In a surface with flour, roll the little balls
   flat.

8. On an oily and hot pan, throw one of the flatbreads, wait until it bubbles up ant it toasts a
   little bit. Spread some oil on the uncooked side and turn around.



## Changelog

#### First attempt: _2021-02-05_

* It was delicious, as always.
* Ate it with some salad made with spinach, cabbage, broccoli and carrot. 
