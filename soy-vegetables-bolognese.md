# Recipe Template

I have been trying to make the perfect veggie bolognese that can perfectly mimic a tasty beef-based
  one. I believe this one would trick anyone, as it has the perfect balance between flavour and
  consistency. 

`#vegan` `#bolognese`


## Ingredients

**Massive pan of bolognese sauce, for 6 plates of spaghetti.**

* Two onions
* Two carrots
* A yellow/red pepper
* A green Italian pepper
* A small zucchini
* A cup of textured soy protein
* A big jar of fried tomato sauce
* A glass of wine
* Olive oil
* Salt
* Chili flakes
* Dried oregano or mixed Italian herbs

## Method 

1. The secret to a perfect texture is to cut all vegetables carefully in really really small pieces.
   Start with the onions, chop them thinly and throw them into a big pan with a bit of olive oil so
   that it doesn't get stuck. Fry them at slow heat till it becomes soft.
2. If the soy protein is dry, put it in a bowl of hot water for it to swell and soften.
3. Finely chop and add the other vegetables one by one in order from harder to softer. First cut and
   throw in the carrot, then the peppers, finally the zucchini.
4. Throw in the glass of wine, a pinch of salt and a teaspoon of chilli flakes. Wait till the wine
   reduces and lower down to slow heat. The vegetables should cook slowly till they are very soft.
5. Strain the soy very well, squeezing it with some pressure so that it dries as much as possible.
   Then chop it in thin and irregular pieces. The pieces should mimic the texture of mince meat.
6. When the vegetables are all soft, throw the soy in and mix it well all together. Add the tomato
   sauce, mix it and cover it. Let it simmer for 20 minutes, being careful that it doesn't get
   stuck. If it becomes too dry, add a bit more water and mix.
7. Taste it and add salt and oregano if necessary.

## Changelog

#### First attempt: _2021-10-02_

* I've made this to fill up aubergines. For that, I've cut the aubergines in half, cut a not too
thin grid on their flesh, put salt and olive oil and bake them at 150º for 30-40 minutes. Once the
aubergines were cooked, I scooped a bit of their flesh (not all) with a spoon so that I could fill
them up with the bolognese. I smashed the scooped flesh and mixed it with the bolognese. A bit of
cheese on top and some extra 10 minutes of oven and done!
* Delicious and full of veggies.
