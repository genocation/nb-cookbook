# Recipe Template

Short description

`#tag1` `#tag2` `#tag3`


## Ingredients

**For N people/rations/units**

* Flour (300gr)
* Water (200ml)

## Method 

1. Step one
2. Step two

## Changelog

#### First attempt: _YYYY-MM-DD_

* Used ingredient 2 instead of ingredient 1
* It was awesome
