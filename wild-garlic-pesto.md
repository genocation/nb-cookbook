# Wild Garlic Pesto 

Wild garlic (Allium ursinum) happens to be extremely common in Asturias. Here it's commonly referred
to as "bear garlic", as it grows near rivers in early spring and it's the first thing that the bears
come down into the valleys to eat, after their hibernation. The first time I tried wild garlic I was
blown by its rich garlic-like flavour but also its freshness, and I was shocked that this plant is
not being widely grown and sold -- at least in Spain.

My first experiment with wild garlic gathered from the nature is an extremely delicious and easy
pesto. The wild garlic was gathered from the Biescona beech forest on 5th April 2022.

`#pesto` `#wild food`

![Wild garlic in nature](./assets/wild-garlic.jpg)

## Ingredients

**For a medium-sized jar (see pic below)**

* One bag of young and fresh wild garlic leaves
* Walnuts (a fistful, other types of nut will also do)
* Parmesan cheese (or other strong-flavoured cheese, grated)
* Olive oil
* Salt
* Lemon juice (a couple of squishes)

## Method 

1. Wash the wild garlic leafs very well and strain them thoroughly. I also pressed an absorbent
   towel over the leafs so that the pesto would not be too watery. 
2. Use a food blender to blend the leaves with the nuts and the cheese. You can throw in a bit of
   oil to make it easier to blend.
3. Once you have a paste, add some more olive oil and keep blending for a while till you have a nice 
   creamy consistency.
4. Add salt to taste. Last, add a couple of squishes of lemon juice and blend together once more
   time.

![Wild garlic pesto](./assets/wild-garlic-pesto.jpg)

## Changelog

#### First attempt: _2022-04-05_

* I normally use walnuts for the pesto, because pine nuts are extremely expensive and because our
  walnut tree is an overproducer and I don't know what to do with so many walnuts.
* Generally pesto has garlic and many wild garlic pesto recipes online also add garlic to the mix. I
  was scared of making it TOO garlicky, and I'm glad I did not add any garlic because the result is
  quite strong flavoured already, but with a fresh note. 
* IT'S SO GREEN OMG.
* I am not sure how well this keeps in the fridge, so I will buy some freezing bags and put some
  into the freezer.
