# Tomato Chutney

My annual chutney varies and is usually largely improvised, depending on whats available, but always made with freshly picked tomatoes from the end of season. It doesnt matter what state they're in, hard, soft, slightly moldy, yellow, red, green, cherry, bull, just chuck them in a pot and boil until delicious!

## Ingredients

**Makes around 3kg, I recommend several large jars (for personal consumption), and several little ones (presents for friends)!**

- 3kg freshly picked homegrown tomatoes
- 5 large onions
- 2+ apples
- 750ml apple cider vingear
- 5 or more cloves of garlic
- 10cm or more ginger
- 750g dark brown sugar

_Optional extras:_
- Raisins / Sultanas / other dried fruit (particularly delicious with with green tomatoes)
- Mustard seeds
- Cinnamon

## Method

1. Take a (very) large cooking pot, melt the brown sugar on a low temperature, stirring regularly, until it forms a caramel. Be careful not to burn it, or you'll be starting all over again! This takes a while, so you can start chopping the ingredients.

2. Once a caramel is formed, add the vinegar. The caramel will harden when the cold vinegar is poured over it. Don't worry! Slowly bring the vinegar to the boil, the sugar will remelt once the liquid is hot!

3. As things are ready, simply throw them into the pot. First chop the onions in lengthy strips. Depending on how much crunch you like in your chutney, chop small or large pieces. I tend to slice the garlic so you get a cross-section of the core. While the ginger and apple I prefer in smaller pieces so it mixes more thoroughly.

4. Bring to the boil and simmer for 1 or more hours. Now is the perfect time to go and water the plants, or run a little errand.

5. 15 minutes before finishing, lay out several other large pots with your glass jars. Boil the kettle a few times and sterilise all the jars and lids. Fill them all the way to the top with water. Make sure the water is off the boil, you don't want your jars to shatter!

6. Taking one jar at a time, empty it of boiling water, then add the cooked chutney using a ladle, holding it with a dish cloth. Wipe the edges with a wet cloth before tightening the lid, this stops the jars being sticky.

7. Set aside to rest and cool. As the jars cool they'll seal. You should occasionally hear a little pop...

## Changelog

#### First recorded attempt: _2021-09-09_
- Used a mixture of larger red tomatoes and small sweet yellow cherry tomatoes. Its quite delicious, very sweet! Got 11 jars in total, including 3 large ones all for me (and you...). Can't wait for it to cool so I can have it with cheese and oat cakes.
