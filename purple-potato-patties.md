# Purple potato patties 

This recipe started from the desire to eat a tasty home-made vegetarian burger and use some
left-over purple potatoes to something original. In the process, I used pretty ingredients that I
normally have around at home, and improvised the recipe being careful of creating a dough that would
not be too watery or soft. This is one of the particularities of purple potatoes: they are very
dense and have a floury texture. 

Following this recipe I made 2 burger-size patties and 12 medium-sized ones which I froze. This
ended up being extremely practical, as I just needed to heat a bit of oil and throw a couple of
those inside the pan to have a delicious meal.

The spices and seeds that I threw in were the ones that I had around, but I am sure it would be
delicious with any kind of spices, herbs and seeds that are available at the moment.

`#patty` `#burger` `#falafel` `#wrap` `#vegan`


## Ingredients

**For 16 medium-sized or 8 burger-sized patties**

* Purple potatoes (400gr)
* Boiled chickpeas (400gr)
* One large spring onion (around 100gr, very thinly chopped)
* Two garlic cloves (very thinly chopped)
* Parsley (a bundle, also thinly chopped)
* Lemon juice (half a lemon) 
* Tahini paste (a couple of spoons)
* Seeds (sesame, linen, sunflower and chia seeds)
* Spices (Ras el Hanout, turmeric and cumin)
* Salt
* Olive oil

**For the breading**

* Chickpea flour (or any other)
* Plant-based milk (oat milk)
* Breadcrumbs


## Method 

1. Mash the purple potatoes with a potato masher or a fork. If you use a food processor, don't blend
   it too fine. Do the same with the chickpeas and mix it all together. It's really important to
   mash it leaving some texture and pieces of potato and chickpea.

2. Chop the onion and the garlic really really thin and throw it in the mix. Add the parsley and the
   seeds. Add a couple of big spoons of tahini paste and mix it all together.

3. Once you have a paste, you just need to season it with patience. Add the amount of spices that
   you see fit (a tea spoon of Ras el Hanout, another tea spoon of turmeric, and another of cumin
   seeds did it for me). You can get creative here! Some chilli flakes for a bit of joy, some thinly
   chopped sundried tomato for a pinch of umami... the potato and chickpeas aren't strong flavoured,
   so anything that you add here will be the key for the final flavor.

4. Finally add salt, mix and taste, and do the same with a couple of squishes of lemon juice. The
   texture of the mix should be solid enough to be able to make balls that easily keep their shape.

5. Let's form and bread the balls. Take a fistful of mix, like a ball of the size of a lemon, press
   it together firmly and make a falafel-shaped patty. Take double or 1.5 that to make a burger.
   Prepare one plate with chickpea flour, one bowl with milk and another plate with breadcrumbs. The
   breading order should be: one full covering of flour, then milk, then flour, then milk, and
   finally breadcrumbs. Make sure that the whole patty is homogeneously covered in breadcrumbs.

6. Deep fry them in very hot oil or store them in the freezer separated by film so that they don't
   get stuck.


## Changelog

#### First attempt: _2022-03-01_

* Made a large patty to have as a burger. Instead of coating it with double layer of flour I did
  with one only layer, and the crust wasn't as crunchy and awesome as it should. After this failure,
  I re-breaded them all with double-coating.

#### _2022-04-02_

* Cooked the frozen leftovers for a beach picnic and had them with:
  * Home-made flatbreads (see [flatbreads with mole recipe](./flatbreads-with-mole.md) for the
  flatbread recipe)
  * Guacamole
  * Lettuce
  * Tomatoes
  * Pickled onions
  * Mayonnaise and mustard sauce
* And yeah, they were delicious, even cold.
